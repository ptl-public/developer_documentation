| variable                | description                                   | example                                                                       |
|-------------------------|-----------------------------------------------|-------------------------------------------------------------------------------|
| `portal.domain`         | The fully qualified domain name of the Portal | `8271dd.p.getportal.org`                                                      |
| `portal.id`             | The full-length hash-ID of the Portal         | `8271ddlqxa [...] 8s598f2`                                                    |
| `portal.short_id`       | The first six digits of the Portal's hash-ID  | `8271dd`                                                                      |
| `portal.public_key_pem` | The Portal's public key in PEM format         | `-----BEGIN PUBLIC KEY-----\nMIICI [...] wEAAQ==\n-----END PUBLIC KEY-----\n` |
