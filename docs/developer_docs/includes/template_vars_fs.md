| variable          | description                                        | example                                  |
|-------------------|----------------------------------------------------|------------------------------------------|
| `fs.app_data`     | The absolute path to your app's directory          | `/home/portal/user_data/app_data/my-app` |
| `fs.all_app_data` | The absolute path to the app data parent directory | `/home/portal/user_data/app_data`        |
| `fs.shared`       | The absolute path to the directory for shared data | `/home/portal/user_data/shared`          |
