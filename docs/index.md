---
hide:
  - navigation
  - toc
---

# Portal

Portal is a consumer-grade cloud personal cloud computer.

For a general overview of Portal and its concepts, see the [Portal Overview](overview).

If you are a developer and want to know more about developing or adapting applications for Portal, see the [Portal Developer Docs](developer_docs/overview.md).

We also have a [blog](blog) and some [user guides](user_guides/password_management.md).
